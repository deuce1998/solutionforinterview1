//Task 1
const hillHeight = 100,
speedUp = 50,
speedDown = 30;

function countDays(hillHeight,speedUp,speedDown){
    let currentHeight = 0,
    numberIteration = 0;
    const difference = speedUp - speedDown;
    if(difference < 0 || difference == 0){
        return 0;
    }
    else{
        while(currentHeight + speedDown <= hillHeight){
            currentHeight += difference;
            numberIteration++;
        }
        return numberIteration;
    }
}

console.log(countDays(hillHeight, speedUp, speedDown));

//Task 2

function getCountHandshakes(countPeople){
    let countHandshakes = 0
    countHandshakes = countPeople <= 1 ?  0 :
    countPeople * (countPeople - 1) / 2;

    return countHandshakes;
}

console.log(getCountHandshakes(10));

//Task 3

function removeDuplicatWord(fullString){
   const inputArray = fullString.split(',');
   const uniqueArray = [...new Set(inputArray)];

   return uniqueArray.join(',');
}
